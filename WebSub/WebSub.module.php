<?php namespace ProcessWire;

/**
 * ProcessWire 'Hello world' demonstration module
 *
 * Demonstrates the Module interface and how to add hooks.
 * 
 * See README file for further links regarding module development.
 * 
 * This file is licensed under the MIT license
 * https://processwire.com/about/license/mit/
 * 
 * ProcessWire 3.x, Copyright 2016 by Ryan Cramer
 * https://processwire.com
 *
 */

class WebSub extends WireData implements Module {

    /**
     * getModuleInfo is a module required by all modules to tell ProcessWire about them
     *
     * @return array
     *
     */
    public static function getModuleInfo() {

        return array(

            // The module's title, typically a little more descriptive than the class name
            'title' => 'WebSub', 

            // version number 
            'version' => 1, 

            // summary is brief description of what this module is
            'summary' => 'WebSub support for ProcessWire.',
            
            // Optional URL to more information about the module
            // 'href' => 'https://processwire.com',

            // singular=true: indicates that only one instance of the module is allowed.
            // This is usually what you want for modules that attach hooks. 
            'singular' => true, 

            // autoload=true: indicates the module should be started with ProcessWire.
            // This is necessary for any modules that attach runtime hooks, otherwise those
            // hooks won't get attached unless some other code calls the module on it's own.
            // Note that autoload modules are almost always also 'singular' (seen above).
            'autoload' => true, 
        
            // Optional font-awesome icon name, minus the 'fa-' part
            'icon' => 'smile-o', 
            );
    }

    /**
     * Initialize the module
     *
     * ProcessWire calls this when the module is loaded. For 'autoload' modules, this will be called
     * when ProcessWire's API is ready. As a result, this is a good place to attach hooks. 
     *
     */
    public function init() {

        // add a hook after the $pages->save, to issue a notice every time a page is saved
        $this->pages->addHookAfter('save', $this, 'notifyWebSubHub'); 
    }


    /**
     * Example1 hooks into the pages->save method and displays a notice every time a page is saved
     * 
     * @param HookEvent $event
     *
     */
    public function notifyWebSubHub($event) {
        /** @var Page $page */
        $page = $event->arguments[0];
        
        if ($page->isUnpublished()) {
            return;
        }

        // load configuration
        $cfg = $this->modules->getConfig('WebSub');
        if (!in_array($page->template->id, $cfg['entryTemplates'])) {
            return;
        }

        $feeds = wire('pages')->findMany("template=" . implode('|', $cfg['feedTemplates']));
        $feed_urls = array();
        
        foreach ($feeds as $f) {
            $q = $f->get('query');
            if (!empty($q)) {
                $need_update = $page->matches($q);
                if ($need_update) {
                    array_push($feed_urls, $f->httpUrl);
                    $log_msg = "%s found in feed query for %s";
                } else {
                    $log_msg = "%s not found in feed query for %s";
                }
                if ($cfg['verbose_logging']) {
                    $this->log->save('websub-sent', sprintf($log_msg, $page->path, $f->path));
                }
            }
        }

        $http = new WireHttp();
        $http->setHeader('Content-Type', 'application/x-www-form-urlencoded');
        if ($cfg['hub_arrays']) {
            $this->log->save('websub-sent', sprintf('Sending WebSub POSTs using arrays to %s', $cfg['hub']));
            $response = $http->post($cfg['hub'], [
                'hub.mode' => 'publish',
                'hub.url' => $feed_urls,
            ]);
            if ($response !== false) {
                $this->log->save('websub-sent', "Successful response: " . $this->sanitizer->entities($response));
            }
            else {
                $this->log->save('websub-sent', "HTTP Request failed: " . $http->getError()); 
            }
        }
        else {
            $this->log->save('websub-sent', sprintf('Sending WebSub POSTs individually to %s', $cfg['hub']));
            foreach ($feed_urls as $fu) {
                $http->post($cfg['hub'], [
                    'hub.mode' => 'publish',
                    'hub.url' => $fu,
                ]);
            }
            if ($response !== false) {
                $this->log->save('websub-sent', "Successful response: " . $this->sanitizer->entities($response));
            }
            else {
                $this->log->save('websub-sent', "HTTP Request failed: " . $http->getError()); 
            }
        }
        
        $this->message("WebSub feeds updated for {$page->path}."); 
    }
    
}
