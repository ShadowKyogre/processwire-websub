<?php

namespace ProcessWire;

class WebSubConfig extends ModuleConfig {

    public function __construct() {
        $tpls = array();
        foreach(wire('templates') as $t) {
            if (!($t->flags & Template::flagSystem)) {
                $tpls[$t->id] = $t->name;
            }
        }

        $flds = array();
        foreach(wire('fields')->find('type=FieldtypeSelector') as $f) {
            if (!($f->flags & Template::flagSystem)) {
                $flds[$f->id] = $f->name;
            }
        }


        $this->add(array(
            array(
                'name' => 'instructions',
                'type' => 'markup',
                'label' =>  __('Instructions', __FILE__),
                'markupText' => __('For this module to work, please select the templates to allow this module to find feeds to update.', __FILE__),
            ),

            array(
                'name' => 'hub',
                'type' => 'text',
                'label' => 'WebSub Hub',
                'description' => 'Enter the URL',
                'required' => true,
                'value' => 'https://pubsubhubbub.superfeedr.com/',
                'columnWidth' => 50,
            ),

            array(
                'name' => 'hub_arrays',
                'label' => 'Send Feed URLs in an array?',
                'description' => 'Normally WebSub hubs support one URL per ping. Check this if yours supports array pings.',
                'type' => 'checkbox',
                'value' => true,
                'columnWidth' => 50,
            ),

            array(
                'name' => 'feedTemplates',
                'type' => 'asmSelect',
                'label' => __('Feed templates', __FILE__),
                'description' => __('Your saved pages will be checked against these page types.', __FILE__),
                'sortable' => false,
                'options' => $tpls,
                'value' => null,
                'columnWidth' => 33,
            ),

            array(
                'name' => 'feedSelector',
                'type' => 'asmSelect',
                'label' => __('Feed field', __FILE__),
                'description' => __('The selector field to use.', __FILE__),
                'sortable' => false,
                'options' => $flds,
                'value' => null,
                'columnWidth' => 33,
            ), 

            array(
                'name' => 'entryTemplates',
                'type' => 'asmSelect',
                'label' => __('Entry templates', __FILE__),
                'description' => __('Saved pages to check for WebSub update pushes.', __FILE__),
                'sortable' => false,
                'options' => $tpls,
                'value' => null,
                'columnWidth' => 33,
            ),

            array(
                'name' => 'verbose_logging',
                'label' => 'Enable verbose logging',
                'description' => 'Verbose logging includes websub HTTP requests. Regular logging will only include errors.',
                'type' => 'checkbox',
                'value' => true,
                'columnWidth' => 100,
            ),
        ));
    }
}